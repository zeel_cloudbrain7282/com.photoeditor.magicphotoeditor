package com.photoeditor.magicphotoeditor.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.photoeditor.magicphotoeditor.R;

import java.io.File;

public class img_share_Activity extends AppCompatActivity {
ImageView img_back,share_img;
LinearLayout ll_whatapp,ll_facebook,ll_intagram,ll_more;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_img_share_);
        img_back=findViewById(R.id.img_back);
        ll_whatapp=findViewById(R.id.ll_whatapp);
        ll_facebook=findViewById(R.id.ll_facebook);
        ll_intagram=findViewById(R.id.ll_intagram);
        ll_more=findViewById(R.id.ll_more);
        share_img=findViewById(R.id.share_img);
        String imagePath = getIntent().getStringExtra("imgpath");
        BitmapFactory.Options options = new BitmapFactory.Options();
        Bitmap bitmap = BitmapFactory.decodeFile(imagePath, options);
        share_img.setImageBitmap(bitmap);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        ll_whatapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShareImage("WhatsApp", "com.whatsapp", imagePath);

            }
        });
        ll_facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShareImage("Facebook", "com.facebook.katana", imagePath);

            }
        });
        ll_intagram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShareImage("Instagram", "com.instagram.android", imagePath);

            }
        });
        ll_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    String shareMsg = "Download " + getString(R.string.app_name) + " App : " + Uri.parse("https://play.google.com/store/apps/details?id=" + getPackageName());
                    File mFile = new File(imagePath);
                    Uri uri = Uri.fromFile(mFile);
                    StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                    StrictMode.setVmPolicy(builder.build());
                    Intent imageshare = new Intent(Intent.ACTION_SEND);
                    imageshare.setType("image/*");
                    imageshare.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    imageshare.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name));
                    imageshare.putExtra(Intent.EXTRA_TEXT, shareMsg);
                    imageshare.putExtra(Intent.EXTRA_STREAM, uri);
                    startActivity(imageshare);
                } catch (Exception e) {
                }
            }
        });
    }
    public void ShareImage(String name, String shareFlag, String createdImage) {
        File mFile = new File(createdImage);
        if (mFile.exists()) {
            PackageManager pm = getPackageManager();
            boolean isInstalled = isAppInstall(shareFlag, pm);

            if (isInstalled) {
                try {
                    StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                    StrictMode.setVmPolicy(builder.build());

                    String shareMsg = "Download " + getString(R.string.app_name) + " App : " + Uri.parse("https://play.google.com/store/apps/details?id=" + getPackageName());

                    Uri uri = Uri.fromFile(mFile);
                    Intent imageshare = new Intent(Intent.ACTION_SEND);
                    imageshare.setType("image/*");
                    if (shareFlag != null) imageshare.setPackage(shareFlag);
                    imageshare.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    imageshare.putExtra(Intent.EXTRA_STREAM, uri);
                    imageshare.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name));
                    imageshare.putExtra(Intent.EXTRA_TEXT, shareMsg);
                    startActivity(imageshare);
                } catch (Exception e) {
                }
            } else {
                Toast.makeText(img_share_Activity.this, name + " is not Installed.", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(img_share_Activity.this, "Something went wrong...!!", Toast.LENGTH_SHORT).show();
        }
    }
    public static boolean isAppInstall(String packageName, PackageManager packageManager) {
        boolean found = true;
        try {
            packageManager.getPackageInfo(packageName, 0);
        } catch (PackageManager.NameNotFoundException e) {

            found = false;
        }

        return found;
    }
}