package com.photoeditor.magicphotoeditor.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.photoeditor.magicphotoeditor.R;

import de.hdodenhof.circleimageview.CircleImageView;


public class BgColorAdapter extends BaseAdapter {
    int[] bgColor;
    LayoutInflater inflter;
    Activity mContext;

    public long getItemId(int i) {
        return (long) i;
    }

    public BgColorAdapter(int[] iArr, Activity activity) {
        this.bgColor = iArr;
        this.mContext = activity;
        this.inflter = LayoutInflater.from(activity);
    }

    public int getCount() {
        return this.bgColor.length;
    }

    public Object getItem(int i) {
        return Integer.valueOf(i);
    }

    @SuppressLint({"ViewHolder", "InflateParams"})
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater layoutInflater = (LayoutInflater) this.mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (layoutInflater != null) {
            view = layoutInflater.inflate(R.layout.item_text_bgcolor, null);
            CircleImageView imageView =  view.findViewById(R.id.imgBg);


            if (i == 0) {
                imageView.setImageResource(R.drawable.bgtrans);
            } else {
                imageView.setColorFilter(this.bgColor[i]);
            }
            imageView.requestLayout();
            return view;
        }
        throw new AssertionError();
    }
}
