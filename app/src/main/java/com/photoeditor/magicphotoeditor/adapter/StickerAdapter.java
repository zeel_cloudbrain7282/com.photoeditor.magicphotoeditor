package com.photoeditor.magicphotoeditor.adapter;

import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.photoeditor.magicphotoeditor.R;
import com.photoeditor.magicphotoeditor.activity.Edit_image_Activity;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


public class StickerAdapter extends RecyclerView.Adapter<StickerAdapter.MainClick_DataHolder> {

    ArrayList<Drawable> mySongs;
    LayoutInflater inflater;
    Edit_image_Activity mainClick_activity;
    public StickerAdapter(Edit_image_Activity mainClick_activity, List<Drawable> listStickers) {
        this.mainClick_activity = mainClick_activity;
        this.mySongs= (ArrayList<Drawable>) listStickers;
        inflater = LayoutInflater.from(mainClick_activity);
    }

    @NonNull
    @Override
    public StickerAdapter.MainClick_DataHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.sticker_item, parent, false);
        return new MainClick_DataHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull StickerAdapter.MainClick_DataHolder holder, int position) {
        holder.photo.setImageDrawable(mySongs.get(position));
    }

    @Override
    public int getItemCount() {
        return mySongs.size();
    }
    class MainClick_DataHolder extends RecyclerView.ViewHolder {



        TextView main_txt;

        ImageView photo;
        public MainClick_DataHolder(@NonNull View itemView) {
            super(itemView);
            photo=itemView.findViewById(R.id.photo);


        }
    }
}
