package com.photoeditor.magicphotoeditor.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.photoeditor.magicphotoeditor.R;
import com.photoeditor.magicphotoeditor.Sticker.BitmapStickerIcon;
import com.photoeditor.magicphotoeditor.Sticker.DeleteIconEvent;
import com.photoeditor.magicphotoeditor.Sticker.DrawableSticker;
import com.photoeditor.magicphotoeditor.Sticker.FlipHorizontallyEvent;
import com.photoeditor.magicphotoeditor.Sticker.Sticker;
import com.photoeditor.magicphotoeditor.Sticker.StickerView;
import com.photoeditor.magicphotoeditor.Sticker.ZoomIconEvent;
import com.photoeditor.magicphotoeditor.adapter.GlareAdapter;
import com.photoeditor.magicphotoeditor.onClickRecycle;
import com.photoeditor.magicphotoeditor.adapter.StickerAdapter;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.lucasr.twowayview.TwoWayView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class Edit_image_Activity extends AppCompatActivity {
    ImageView img_back, img_org, img_light;
    CardView crd_save;
    LinearLayout ll_light, ll_rotate, ll_sticker, ll_stickers, ll_text, ll_detail, main_ll;
    RecyclerView sticker;
    FrameLayout frm_Main;
    TwoWayView hl_Frm;
    private List<Drawable> liststicker = new ArrayList<>();
    public static StickerView fl_Sticker;
    public boolean isLight = false;
    public ArrayList<Integer> frmList;
    private BaseAdapter frameAdapter;
    int rotate = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_image_);
        crd_save = findViewById(R.id.crd_save);
        main_ll = findViewById(R.id.main_ll);
        ll_sticker = findViewById(R.id.ll_sticker);
        ll_rotate = findViewById(R.id.ll_rotate);
        ll_light = findViewById(R.id.ll_light);
        ll_text = findViewById(R.id.ll_text);
        img_back = findViewById(R.id.img_back);
        hl_Frm = findViewById(R.id.hl_Frm);
        img_org = findViewById(R.id.img_org);
        img_light = findViewById(R.id.img_light);
        ll_detail = findViewById(R.id.ll_detail);
        frm_Main = findViewById(R.id.frm_Main);
        sticker = findViewById(R.id.sticker);
        fl_Sticker = (StickerView) findViewById(R.id.fl_Sticker);
        ll_stickers = findViewById(R.id.ll_stickers);
        ll_stickers.setVisibility(View.GONE);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .start(this);

        ll_sticker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll_stickers.setVisibility(View.VISIBLE);
                main_ll.setVisibility(View.VISIBLE);
                sticker.setVisibility(View.VISIBLE);
                ll_detail.setVisibility(View.GONE);
                hl_Frm.setVisibility(View.GONE);
            }
        });
        hl_Frm.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {


                if (Edit_image_Activity.this.isLight) {
                    Edit_image_Activity.this.img_light.setImageResource((Edit_image_Activity.this.frmList.get(position)).intValue());
                    return;
                }
            }
        });

        crd_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fl_Sticker.setLocked(true);
                FileOutputStream fileOutputStream = null;
                File file = getdisc();
                if (!file.exists() && !file.mkdirs()) {
                    Toast.makeText(getApplicationContext(), "sorry can not make dir", Toast.LENGTH_LONG).show();
                    return;
                }
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyymmsshhmmss");
                String date = simpleDateFormat.format(new Date());
                String name = "img" + date + ".jpeg";
                String file_name = file.getAbsolutePath() + File.separator + name;
                File new_file = new File(file_name);
                try {
                    fileOutputStream = new FileOutputStream(new_file);
                    Bitmap bitmap = viewToBitmap(frm_Main, frm_Main.getWidth(), frm_Main.getHeight());
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream);
                    Toast.makeText(getApplicationContext(), "Image Save", Toast.LENGTH_LONG).show();
                    fileOutputStream.flush();
                    fileOutputStream.close();

                    Intent intent = new Intent(Edit_image_Activity.this, img_share_Activity.class);
                    intent.putExtra("imgpath", new_file.getAbsolutePath());
                    startActivity(intent);
                } catch
                (FileNotFoundException e) {

                } catch (IOException e) {

                }
                refreshGallary(file);
            }


            private void refreshGallary(File file) {
                Intent i = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                i.setData(Uri.fromFile(file));
                sendBroadcast(i);
            }

            private File getdisc() {
                File dCimDirPath = new File(Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_DCIM).getAbsolutePath());

                if (!dCimDirPath.exists())
                    dCimDirPath.mkdir();
                File myCreationDir = new File(dCimDirPath, getString(R.string.app_name));
                if (!myCreationDir.exists())
                    myCreationDir.mkdirs();
//                File file= Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
//                Log.e("TAG", "getdisc: "+myCreationDir );
                return myCreationDir;

            }
        });
        ll_rotate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rotate == 1) {
                    img_org.setRotation(90.0f);
                    rotate = 2;
                } else if (rotate == 2) {
                    img_org.setRotation(180.0f);
                    rotate = 3;
                } else if (rotate == 3) {
                    img_org.setRotation(270.0f);
                    rotate = 4;

                } else if (rotate == 4) {
                    img_org.setRotation(360.0f);
                    rotate = 1;
                }
            }
        });
        ll_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Edit_image_Activity.this, Txt_add_Activity.class);
                i.putExtra("number", 0);
                startActivity(i);
            }
        });
        ll_light.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isLight = true;
                overViewLightList();
                main_ll.setVisibility(View.VISIBLE);
            }
        });
        set_rv_sticker();
        sticker_boder_set();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                img_org.setImageURI(resultUri);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
//                Log.e("TAG", "onActivityResult: "+error );
            }
        }


    }

    public void overViewLightList() {
        setArraylistForLight();
        frameAdapter = new GlareAdapter(this, frmList);
        hl_Frm.setAdapter(frameAdapter);
        hl_Frm.setVisibility(View.VISIBLE);
        ll_stickers.setVisibility(View.GONE);
        sticker.setVisibility(View.GONE);
        openDetail();
    }

    public void openDetail() {
        ll_detail.setVisibility(View.VISIBLE);
        TranslateAnimation anim = new TranslateAnimation(0.0f, 0.0f, img_org.getY() + 70.0f, img_org.getY());
        anim.setDuration(300);
        anim.setFillAfter(true);
        ll_detail.startAnimation(anim);
    }

    private void setArraylistForLight() {
        frmList = new ArrayList<>();
        frmList.add(Integer.valueOf(R.drawable.flare_06));
        frmList.add(Integer.valueOf(R.drawable.flare_02));
        frmList.add(Integer.valueOf(R.drawable.flare_03));
        frmList.add(Integer.valueOf(R.drawable.flare_04));
        frmList.add(Integer.valueOf(R.drawable.flare_05));
        frmList.add(Integer.valueOf(R.drawable.flare_01));
    }

    private void set_rv_sticker() {
        try {
            String foldername = "emoji";

            String[] list = getAssets().list(foldername);
            for (String str : list) {
                AssetManager assetManager = getAssets();
                String stringBuilder = foldername + File.separator + str;
                liststicker.add(Drawable.createFromStream(assetManager.open(stringBuilder), null));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        StickerAdapter mainClick_adapter = new StickerAdapter(Edit_image_Activity.this, liststicker);
        sticker.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        sticker.setAdapter(mainClick_adapter);
        sticker.addOnItemTouchListener(
                new onClickRecycle(getApplicationContext(), new onClickRecycle.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        fl_Sticker.addSticker(new DrawableSticker(liststicker.get(position)), Sticker.Position.CENTER);
                    }
                })
        );
    }

    private void sticker_boder_set() {
        final BitmapStickerIcon deleteIcon = new BitmapStickerIcon(ContextCompat.getDrawable(this,
                R.drawable.sticker_ic_close_white_18dp),
                BitmapStickerIcon.LEFT_TOP);
        deleteIcon.setIconEvent(new DeleteIconEvent());

        BitmapStickerIcon zoomIcon = new BitmapStickerIcon(ContextCompat.getDrawable(this,
                R.drawable.sticker_ic_scale_white_18dp),
                BitmapStickerIcon.RIGHT_BOTOM);
        zoomIcon.setIconEvent(new ZoomIconEvent());

        BitmapStickerIcon flipIcon = new BitmapStickerIcon(ContextCompat.getDrawable(this,
                R.drawable.sticker_ic_flip_white_18dp),
                BitmapStickerIcon.RIGHT_TOP);
        flipIcon.setIconEvent(new FlipHorizontallyEvent());
        fl_Sticker.setIcons(Arrays.asList(deleteIcon, zoomIcon, flipIcon/*, heartIcon*/));

        fl_Sticker.setBackgroundColor(Color.TRANSPARENT);
        fl_Sticker.setLocked(false);
        fl_Sticker.setConstrained(true);


        fl_Sticker.setOnStickerOperationListener(new StickerView.OnStickerOperationListener() {
            @Override
            public void onStickerAdded(@NonNull Sticker sticker) {

            }

            @Override
            public void onStickerClicked(@NonNull Sticker sticker) {
            }

            @Override
            public void onStickerDeleted(@NonNull Sticker sticker) {
            }

            @Override
            public void onStickerDragFinished(@NonNull Sticker sticker) {
            }

            @Override
            public void onStickerTouchedDown(@NonNull Sticker sticker) {
            }

            @Override
            public void onStickerZoomFinished(@NonNull Sticker sticker) {
            }

            @Override
            public void onStickerFlipped(@NonNull Sticker sticker) {
            }

            @Override
            public void onStickerDoubleTapped(@NonNull Sticker sticker) {
            }
        });

    }

    private static Bitmap viewToBitmap(View view, int widh, int hight) {
        Bitmap bitmap = Bitmap.createBitmap(widh, hight, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);
        return bitmap;
    }

}