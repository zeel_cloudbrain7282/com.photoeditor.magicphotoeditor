package com.photoeditor.magicphotoeditor.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.photoeditor.magicphotoeditor.R;
import com.photoeditor.magicphotoeditor.Sticker.DrawableSticker;
import com.photoeditor.magicphotoeditor.Sticker.Sticker;
import com.photoeditor.magicphotoeditor.adapter.BgColorAdapter;
import com.photoeditor.magicphotoeditor.adapter.ColorAdapter;
import com.photoeditor.magicphotoeditor.adapter.FontAdapter;

import java.io.IOException;

public class Txt_add_Activity extends AppCompatActivity implements View.OnClickListener {
     ImageView img_back;
     CardView txt_done;
     LinearLayout edit_font_ll, edit_color_ll, edit_shadow_ll, edit_backgro_ll, edit_opacity_ll;
     ImageView edit_img, edit_color_img, edit_shadow_img, edit_backgro_img, edit_opacity_img;
     TextView edit_txt, edit_color_txt, edit_shadow_txt, edit_backgro_txt, edit_opacity_txt;
    public static Bitmap bmap_text_new = null;
     EditText edit_filter;
     LinearLayout screenshot_ll;
     LinearLayout edit_gridfont_ll, edit_gridcolor_ll, edit_gridback_ll, edit_gridopacity_ll;
     GridView gridFont, gridTextColor, gridTextBg;
     SeekBar seek_bar_opacity;
    int alpha_bg = 255;
    boolean flagTempShadow;

    String[] listfont;
    public static int[] fontColorArray;
    public static int[] fontBgColorArray;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_txt_add_);

        img_back = findViewById(R.id.img_back);
        txt_done = findViewById(R.id.txt_done);

        edit_font_ll = findViewById(R.id.edit_font_ll);
        edit_img = findViewById(R.id.edit_img);
        edit_txt = findViewById(R.id.edit_txt);
        edit_color_ll = findViewById(R.id.edit_color_ll);
        edit_color_img = findViewById(R.id.edit_color_img);
        edit_color_txt = findViewById(R.id.edit_color_txt);
        edit_shadow_ll = findViewById(R.id.edit_shadow_ll);
        edit_shadow_img = findViewById(R.id.edit_shadow_img);
        edit_shadow_txt = findViewById(R.id.edit_shadow_txt);
        edit_backgro_ll = findViewById(R.id.edit_backgro_ll);
        edit_backgro_img = findViewById(R.id.edit_backgro_img);
        edit_backgro_txt = findViewById(R.id.edit_backgro_txt);
        edit_opacity_ll = findViewById(R.id.edit_opacity_ll);
        edit_opacity_img = findViewById(R.id.edit_opacity_img);
        edit_opacity_txt = findViewById(R.id.edit_opacity_txt);

        edit_filter = findViewById(R.id.edit_filter);
        screenshot_ll = findViewById(R.id.screenshot_ll);
        seek_bar_opacity = findViewById(R.id.seek_bar_opacity);
        seek_bar_opacity.setProgress(255);
        seek_bar_opacity.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {


                edit_filter.setAlpha((float) progress / (float) (seekBar.getMax()));
                alpha_bg = progress;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        edit_gridfont_ll = findViewById(R.id.edit_gridfont_ll);
        edit_gridcolor_ll = findViewById(R.id.edit_gridcolor_ll);
        edit_gridback_ll = findViewById(R.id.edit_gridback_ll);
        edit_gridopacity_ll = findViewById(R.id.edit_gridopacity_ll);

        gridFont = findViewById(R.id.gridFont);
        gridTextColor = findViewById(R.id.gridTextColor);
        gridTextBg = findViewById(R.id.gridTextBg);
        loadFilterColors();

        hideAndShowLayout(edit_gridfont_ll);

        img_back.setOnClickListener(this);
        txt_done.setOnClickListener(this);

        edit_font_ll.setOnClickListener(this);
        edit_color_ll.setOnClickListener(this);
        edit_shadow_ll.setOnClickListener(this);
        edit_backgro_ll.setOnClickListener(this);
        edit_opacity_ll.setOnClickListener(this);
        loadTextFonts();
        loadTextColors();
        loadTextBgColors();

    }
    @Override
    public void onClick(View view) {

        closeKeyboard();
        int v = view.getId();
        boolean z = false;
        if (v != R.id.txt_done) {
            switch (v) {

                case R.id.img_back:
                    onBackPressed();
                    return;

                case R.id.edit_font_ll:
                    hideAndShowLayout(edit_gridfont_ll);
                    return;

                case R.id.edit_color_ll:
                    hideAndShowLayout(edit_gridcolor_ll);
                    return;

                case R.id.edit_shadow_ll:
                    edit_gridfont_ll.setVisibility(View.GONE);
                    edit_gridcolor_ll.setVisibility(View.GONE);
                    edit_gridback_ll.setVisibility(View.GONE);
                    edit_gridopacity_ll.setVisibility(View.GONE);
                    if (flagTempShadow) {
                        edit_filter.setShadowLayer(0.0f, 0.0f, 0.0f, -1);
                    } else {
                        edit_filter.setShadowLayer(3.0f, -1.0f, 1.0f, R.color.black);
                        z = true;
                    }
                    flagTempShadow = z;

                    return;
                case R.id.edit_backgro_ll:
                    hideAndShowLayout(edit_gridback_ll);


                    return;

                case R.id.edit_opacity_ll:
                    hideAndShowLayout(edit_gridopacity_ll);


                    return;

                default:
                    return;
            }
        }

        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        View currentFocus = getCurrentFocus();
        if (currentFocus == null) {
            currentFocus = new View(this);
        }
        if (inputMethodManager != null) {
            inputMethodManager.hideSoftInputFromWindow(currentFocus.getWindowToken(), 0);
        }
        if (edit_filter.getText().toString().length() == 0) {
            Toast.makeText(this, "Please Enter Some Text", Toast.LENGTH_SHORT).show();
            return;
        }
        edit_filter.setCursorVisible(false);
        bmap_text_new = createBitmapFromLayout(screenshot_ll);

        Drawable d = new BitmapDrawable(getResources(), bmap_text_new);

        int number = getIntent().getIntExtra("number", 0);
        if (number == 0) {
            Edit_image_Activity.fl_Sticker.addSticker(new DrawableSticker(d), Sticker.Position.CENTER | Sticker.Position.CENTER);
        }
        finish();

    }
    private void hideAndShowLayout(LinearLayout linearLayout) {
        edit_gridfont_ll.setVisibility(View.GONE);
        edit_gridcolor_ll.setVisibility(View.GONE);
        edit_gridback_ll.setVisibility(View.GONE);
        edit_gridopacity_ll.setVisibility(View.GONE);

        linearLayout.setVisibility(View.VISIBLE);
    }
    private void closeKeyboard() {
        View currentFocus = getCurrentFocus();
        if (currentFocus != null) {
            ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(currentFocus.getWindowToken(), 0);
        }
    }
    public Bitmap createBitmapFromLayout(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        View currentFocus = getCurrentFocus();
        if (currentFocus == null) {
            currentFocus = new View(this);
        }
        if (inputMethodManager != null) {
            inputMethodManager.hideSoftInputFromWindow(currentFocus.getWindowToken(), 0);
        }
        Bitmap createBitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
        view.draw(new Canvas(createBitmap));
        return createBitmap;
    }

    private void loadTextFonts() {
        try {
            this.listfont = getAssets().list("fonts");
            if (this.listfont != null) {
                for (int i = 0; i < this.listfont.length; i++) {
                    String[] strArr = this.listfont;
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.append("fonts/");
                    stringBuilder.append(this.listfont[i]);
                    strArr[i] = stringBuilder.toString();
                }
                this.gridFont.setAdapter(new FontAdapter(this.listfont, this));
                this.gridFont.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        edit_filter.setTypeface(Typeface.createFromAsset(getAssets(), listfont[position]));

                    }
                });
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void loadFilterColors() {
        int i;
        TypedArray obtainTypedArray = getApplication().getResources().obtainTypedArray(R.array.textcolorarray);
        fontColorArray = new int[obtainTypedArray.length()];
        for (i = 0; i < obtainTypedArray.length(); i++) {
            fontColorArray[i] = obtainTypedArray.getColor(i, 0);
        }
        obtainTypedArray.recycle();
        obtainTypedArray = getApplication().getResources().obtainTypedArray(R.array.textcolorbgarray);
        fontBgColorArray = new int[obtainTypedArray.length()];
        for (i = 0; i < obtainTypedArray.length(); i++) {
            fontBgColorArray[i] = obtainTypedArray.getColor(i, 0);
        }
        obtainTypedArray.recycle();
    }

    private void loadTextBgColors() {
        int[] iArr = fontBgColorArray;
        if (iArr != null) {
            gridTextBg.setAdapter(new BgColorAdapter(iArr, this));
            gridTextBg.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    edit_filter.setBackgroundColor(fontBgColorArray[position]);
                }
            });
        }
    }
    private void loadTextColors() {
        int[] iArr = fontColorArray;
        if (iArr != null) {
            this.gridTextColor.setAdapter(new ColorAdapter(iArr, this));
            this.gridTextColor.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    edit_filter.setTextColor(fontColorArray[position]);
                    edit_filter.setHintTextColor(fontColorArray[position]);
                }
            });
        }
    }
}